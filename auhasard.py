#!/usr/bin/python
# -*- coding: utf-8 -*-

#On va avoir besoin de hasard.
import random

# La liste des candidats
liste = []

#On ouvre le fichier contenant tous les noms
source = open("titre.csv","r",encoding="utf8")

#On le lit ligne par ligne
for candidat in source:
  liste.append(candidat)
source.close

#On prend un échantillon de 10 candidats au hasard    
pioche = random.sample(liste,10)
for candidat in pioche:
   print (candidat)
